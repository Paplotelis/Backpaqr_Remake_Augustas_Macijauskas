import React from 'react';

export default React.createContext({
    guides: [],
    currentGuide: null,
    filteredGuides: [],
    getListOfGuidesAction: () => {},
    getGuideAction: () => {},
    filterGuidesAction: () => {},
    deleteGuideFromContext: () => {},
    cableApp: {},
    updateGuideLocations: () => {},
    loggedInGuide: null,
    setLoggedInGuideAction: () => {},
});