export default {
    MAIN: '/',
    GUIDES: '/guides',
    MAP: '/map',
    LOGIN: '/login',
}
