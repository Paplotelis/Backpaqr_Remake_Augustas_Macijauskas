import React, { useEffect, useContext } from 'react';
import * as PropTypes from 'prop-types';

import { MapWrapper } from './components';
import GuidesContext from 'utils/contexts/guidesContext';

export const MapContainer = (props) => {
    const context = useContext(GuidesContext);

    useEffect(() => {
        context.getListOfGuidesAction();
    }, []);

    const { t } = props;

    if (context.guides === []) {
        return null;
    }

    return (
        <MapWrapper
            guidesWithLocations={context.guides}
            t={t}
        />
    );
};

MapContainer.propTypes = {
    t: PropTypes.func.isRequired,
};

export default MapContainer;