import React from 'react';
import * as PropTypes from "prop-types";
import { Map, Marker, GoogleApiWrapper, } from 'google-maps-react';
import moment from 'moment/moment';

import { GOOGLE_API } from 'constants';
import { MapManIcon } from 'styles/images';
import { LoadingContainer } from './LoadingContainer';

export const MapWrapper = (props) => {
    const { guidesWithLocations, t } = props;

    if (guidesWithLocations === null) {
        return null;
    }

    return (
        <Map
            google={props.google}
            initialCenter={{
                lat: 54.6872,
                lng: 25.2797
            }}
            zoom={7}
        >
            {
                guidesWithLocations.map(guide => {
                    return (
                        <Marker
                            key={`marker-${guide.id}`}
                            name={guide.name}
                            title={`${guide.name}${t('misc.markerTitle')}, ${moment(guide.updated_at).format("YYYY-MM-DD, HH:mm")}`}
                            position={{
                                lat: guide.latitude,
                                lng: guide.longitude
                            }}
                            icon={{
                                url: MapManIcon,
                            }}
                        >
                        </Marker>
                    )
                })
            }
        </Map>
    );
};

MapWrapper.propTypes = {
    guidesWithLocations: PropTypes.array,
};

export default GoogleApiWrapper({
    apiKey: (GOOGLE_API.KEY),
    LoadingContainer: LoadingContainer
})(MapWrapper);