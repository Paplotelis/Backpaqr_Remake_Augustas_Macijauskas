import React, { useReducer } from 'react';

import GuidesWebSocket from './GuidesWebSocket';
import i18n from 'core/localization';
import { getListOfGuides, getGuide } from "./api";
import GuidesContext from 'utils/contexts/guidesContext';
import {
    guidesReducer,
    GET_LIST_OF_GUIDES_REQUEST,
    SET_LOGGED_IN_GUIDE_REQUEST,
    GET_GUIDE_REQUEST,
    FILTER_GUIDES_REQUEST,
    DELETE_GUIDE
} from 'features/guides/guidesReducer';

import { Guide1, Guide2, Guide3, Guide4, Guide5, Guide6, Guide7 } from 'styles/images';
// List to map guides so that they could be accessed randomly
const guidesPhotos = {
    guide1: Guide1,
    guide2: Guide2,
    guide3: Guide3,
    guide4: Guide4,
    guide5: Guide5,
    guide6: Guide6,
    guide7: Guide7,
};

// Action Cable setup
import actionCable from 'actioncable'
const CableApp = {};
CableApp.cable = actionCable.createConsumer(`ws://${window.location.hostname}:3000/cable`);

const GuidesState = (props) => {
    const [guidesState, dispatch] = useReducer(guidesReducer, {
        guides: null,
        currentGuide: null,
        filteredGuides: [],
        loggedInGuide: null
    });

    const getListOfGuidesAction = async () => {
        if (guidesState.guides === null) {
            let guides = await getListOfGuides();

            // Generate an image for each guide to be rendered
            guides = guides.map(guide => {
                return {
                    id: guide.id,
                    name: guide.attributes.name,
                    city: guide.attributes.city,
                    latitude: guide.attributes.current_latitude,
                    longitude: guide.attributes.current_longitude,
                    active: guide.attributes.active,
                    created_at: guide.attributes.created_at,
                    updated_at: guide.attributes.updated_at,
                    photo: guidesPhotos[`guide${Math.floor(Math.random() * 7) + 1}`],
                }
            });

            dispatch({ type: GET_LIST_OF_GUIDES_REQUEST, payload: guides });
            dispatch({ type: FILTER_GUIDES_REQUEST, payload: guides });
        }
    };

    const setLoggedInGuideAction = (loggedInGuide) => {
        dispatch({ type: SET_LOGGED_IN_GUIDE_REQUEST, payload: loggedInGuide });
    };

    const getGuideAction = async (guideID) => {
        const result = await getGuide(guideID, 1, 5);

        const mappedGuide = {
            id: result.data.id,
            name: result.data.attributes.name,
            city: result.data.attributes.city,
            latitude: result.data.attributes.current_latitude,
            longitude: result.data.attributes.current_longitude,
            active: result.data.attributes.active,
            created_at: result.data.attributes.created_at,
            updated_at: result.data.attributes.updated_at,
            locations: result.included.map(location => {
                return {
                    id: location.id,
                    latitude: location.attributes.latitude,
                    longitude: location.attributes.longitude,
                    created_at: location.attributes.created_at,
                    updated_at: location.attributes.updated_at,
                };
            }),
        };

        dispatch({ type: GET_GUIDE_REQUEST, payload: mappedGuide });
    };

    const deleteGuideFromContext = () => {
        dispatch({ type: DELETE_GUIDE });
    };

    const filterGuidesAction = async (search) => {
        let filteredGuides = guidesState.guides;

        if (search) {
            filteredGuides = filteredGuides.filter(guide => {
                return (guide.name.toLowerCase().includes(search)
                    || i18n.t(`cities.${guide.city.replace(/\s/g,'')}`).toLowerCase().includes(search));
            });
        }

        dispatch({ type: FILTER_GUIDES_REQUEST, payload: filteredGuides });
    };

    const updateGuideLocations = (update) => {
        // Updating guides list
        const index = guidesState.guides.findIndex(guide => parseInt(guide.id) === update.guide.id);

        const mappedGuide = {
            id: update.guide.id,
            name: update.guide.name,
            city: update.guide.city,
            latitude: update.guide.current_latitude,
            longitude: update.guide.current_longitude,
            active: update.guide.active,
            created_at: update.guide.created_at,
            updated_at: update.guide.updated_at,
            photo: guidesState.guides[index].photo
        };


        const newGuides = [...guidesState.guides];
        newGuides[index] = mappedGuide;
        dispatch({ type: GET_LIST_OF_GUIDES_REQUEST, payload: newGuides });

        // Updating filteredGuides list
        const filteredIndex = guidesState.filteredGuides.findIndex(guide => parseInt(guide.id) === update.guide.id);
        if (filteredIndex > -1) {
            const newFilteredGuides = [...guidesState.filteredGuides];
            newFilteredGuides[filteredIndex] = mappedGuide;
            dispatch({ type: FILTER_GUIDES_REQUEST, payload: newFilteredGuides });
        }

        // Updating currentGuide
        if (update.location && guidesState.currentGuide && parseInt(guidesState.currentGuide.id) === update.guide.id) {
            let newLocations = [...guidesState.currentGuide.locations];
            newLocations.pop();
            newLocations.unshift({
                id: update.location.id.toString(),
                latitude: update.location.latitude,
                longitude: update.location.longitude,
                created_at: update.location.created_at,
                updated_at: update.location.updated_at,
            });

            dispatch({
                type: GET_GUIDE_REQUEST,
                payload: {
                    ...guidesState.currentGuide,
                    id: update.guide.id,
                    name: update.guide.name,
                    city: update.guide.city,
                    latitude: update.guide.current_latitude,
                    longitude: update.guide.current_longitude,
                    active: update.guide.active,
                    created_at: update.guide.created_at,
                    updated_at: update.guide.updated_at,
                    locations: newLocations
                }
            });
        }
    };

    return (
        <GuidesContext.Provider
            value={{
                guides: guidesState.guides,
                currentGuide: guidesState.currentGuide,
                filteredGuides: guidesState.filteredGuides || [],
                getListOfGuidesAction,
                getGuideAction,
                filterGuidesAction,
                deleteGuideFromContext,
                cableApp: CableApp,
                updateGuideLocations,
                loggedInGuide: guidesState.loggedInGuide,
                setLoggedInGuideAction,
            }}
        >
            <GuidesWebSocket />
            {props.children}
        </GuidesContext.Provider>
    );
};

export default GuidesState;
