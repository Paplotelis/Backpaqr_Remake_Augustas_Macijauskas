export const GET_LIST_OF_GUIDES_REQUEST = 'GUIDES/GET_LIST_OF_GUIDES_REQUEST';
export const GET_LIST_OF_GUIDES_SUCCESS = 'GUIDES/GET_LIST_OF_GUIDES_SUCCESS';
export const GET_LIST_OF_GUIDES_ERROR = 'GUIDES/GET_LIST_OF_GUIDES_ERROR';

export const SET_LOGGED_IN_GUIDE_REQUEST = 'GUIDES/SET_LOGGED_IN_GUIDE_REQUEST';

export const GET_GUIDE_REQUEST = 'GUIDES/GET_GUIDE_REQUEST';
export const GET_GUIDE_SUCCESS = 'GUIDES/GET_GUIDE_SUCCESS';
export const GET_GUIDE_ERROR = 'GUIDES/GET_GUIDE_ERROR';

export const DELETE_GUIDE = 'GUIDES/DELETE_GUIDE';

export const FILTER_GUIDES_REQUEST = 'GUIDES/FILTER_GUIDES_REQUEST';
export const FILTER_GUIDES_SUCCESS = 'GUIDES/FILTER_GUIDES_SUCCESS';
export const FILTER_GUIDES_ERROR = 'GUIDES/FILTER_GUIDES_ERROR';

export const guidesReducer = (state, { type, payload }) => {
    switch (type) {
        case GET_LIST_OF_GUIDES_REQUEST:
            return {
                ...state,
                guides: payload
            };

        case SET_LOGGED_IN_GUIDE_REQUEST:
            return {
                ...state,
                loggedInGuide: payload
            };

        case GET_GUIDE_REQUEST:
            return {
                ...state,
                currentGuide: payload
            };

        case DELETE_GUIDE:
            return {
                ...state,
                currentGuide: null
            };

        case FILTER_GUIDES_REQUEST:
            return {
                ...state,
                filteredGuides: payload
            };

        default:
            return state;
    }
};
