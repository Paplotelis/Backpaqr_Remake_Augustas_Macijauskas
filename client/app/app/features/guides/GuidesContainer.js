import React, { Fragment, useState, useEffect, useContext } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import {
    CssBaseline,
    Grid,
    Typography,
    withStyles,
} from '@material-ui/core';

import { MapModal, GuideCard } from './components';
import GuidesContext from 'utils/contexts/guidesContext';

const styles = theme => ({
    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    cardGrid: {
        padding: `${theme.spacing.unit * 8}px 0`,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 6,
    },
});

const GuidesContainer = (props) => {
    const context = useContext(GuidesContext);

    const [mapOpen, setMapOpen] = useState(false);

    useEffect(() => {
        context.getListOfGuidesAction();
    }, []);

    const handleClickOpen = (guideID) => {
        setMapOpen(true);
        context.getGuideAction(guideID);
    };

    const handleClose = () => {
        setMapOpen(false);
        context.deleteGuideFromContext();
    };

    const { classes, t } = props;

    return (
        <Fragment>
            <CssBaseline />
            <MapModal
                open={mapOpen}
                handleClose={handleClose}
                t={t}
            />
            <main>
                {/* Hero unit */}
                <div className={classes.heroUnit}>
                    <div className={classes.heroContent}>
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            { t('guides.header').toUpperCase() }
                        </Typography>
                        <Typography variant="h6" align="center" color="textSecondary" paragraph>
                            { t('guides.subheader') }
                        </Typography>
                    </div>
                </div>
                <div className={classNames(classes.layout, classes.cardGrid)}>
                    {/* End hero unit */}
                    <Grid container spacing={40}>
                        {context.filteredGuides.map(guide => (
                            <GuideCard
                                key={guide.id}
                                guide={guide}
                                handleClickOpen={handleClickOpen}
                                t={t}
                            />
                        ))}
                    </Grid>
                </div>
            </main>
            {/* Footer */}
            <footer className={classes.footer}>
                <Typography variant="h6" align="center" gutterBottom >
                    Backpaqr
                </Typography>
            </footer>
            {/* End footer */}
        </Fragment>
    );
};

GuidesContainer.propTypes = {
    classes: PropTypes.shape({}).isRequired,
    t: PropTypes.func.isRequired,
};

export default withStyles(styles)(GuidesContainer);
