import { axios } from 'core';

export const getListOfGuides = async () => {
    return (await axios.get('guides?sort=-created_at&page[number]=1&page[size]=100')).data.data;
};

export const getGuide = async (id, pageNumber, howManyPerPage) => {
    return (
        await axios.get(
            `guides/${id}?sort=-locations.created_at&page[locations.number]=${pageNumber}&page[locations.size]=${howManyPerPage}&include=locations`
        )).data;
};

export const updateGuide = async (id, body) => {
    return (await axios.put(`guides/${id}`, body)).data;
};