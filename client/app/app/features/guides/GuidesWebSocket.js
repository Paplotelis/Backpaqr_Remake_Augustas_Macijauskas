import React from 'react';

import { CHANNEL } from "constants";
import GuidesContext from 'utils/contexts/guidesContext';

/* not working as functional component? */
class LineWebSocket extends React.Component {
    static contextType = GuidesContext;

    componentDidMount() {
        this.context.cableApp.cable.subscriptions.create({ channel: CHANNEL.GUIDES_CHANNEL }, {
            received: (newGuide) => {
                this.context.updateGuideLocations(newGuide);
            },
        });
    }

    render() {
        return (
            <div />
        );
    }
}
export default LineWebSocket;