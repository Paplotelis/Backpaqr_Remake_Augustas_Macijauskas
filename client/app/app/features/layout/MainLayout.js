import React, { Fragment, useState, useContext } from 'react';
import { Redirect, withRouter, } from "react-router";
import { Route, Switch } from 'react-router-dom';
import { CssBaseline, withStyles, } from '@material-ui/core';
import * as PropTypes from 'prop-types';
import { withTranslation } from "react-i18next";

import { ROUTES } from 'constants';
import { Header } from './components';
import { LoginContainer } from 'features/login';
import { GuidesContainer } from 'features/guides';
import { MapContainer } from 'features/map';
import { ErrorPage } from 'features/error';
import GuidesContext from 'utils/contexts/guidesContext';
import { updateGuide } from 'features/guides/api';

// Styles
const styles = () => ({
    containers: {
        paddingTop: '4.2%',
    }
});

const MainLayout = (props) => {
    const context = useContext(GuidesContext);

    const [geolocationId, setGeolocationId] = useState(null);

    const handleLogin = async (guide) => {
        context.setLoggedInGuideAction(guide);
        await updateGuide(guide.id, {
            "data": {
                "type": "guides",
                "id": guide.id,
                "attributes": {
                    "active": true
                }
            }
        });
        if (navigator.geolocation) {
            const id = navigator.geolocation.watchPosition(async (position) => {
                const { latitude, longitude } = position.coords;
                if (latitude && longitude) {
                    await updateGuide(guide.id, {
                        "data": {
                            "type": "guides",
                            "id": guide.id,
                            "attributes": {
                                "current_latitude": latitude,
                                "current_longitude": longitude,
                            }
                        }
                    });
                }
            });
            setGeolocationId(id);
        }
        props.history.push(ROUTES.GUIDES);
    };

    const handleLogout = async (guide) => {
        context.setLoggedInGuideAction(null);
        await updateGuide(guide.id, {
            "data": {
                "type": "guides",
                "id": guide.id,
                "attributes": {
                    "active": false
                }
            }
        });
        navigator.geolocation.clearWatch(geolocationId);
        props.history.push(ROUTES.GUIDES);
    };

    const handleLanguageChange = (language) => {
        const { i18n } = props;

        i18n.changeLanguage(language);
    };

    const { classes, t, i18n } = props;

    return (
        <Fragment>
            <CssBaseline />
            <Header
                handleLanguageChange={handleLanguageChange}
                t={t}
                language={i18n.language}
                handleLogout={handleLogout}
            />
            <div className={classes.containers}>
                <Switch>
                    <Route path={ROUTES.GUIDES} exact>
                        <GuidesContainer t={t} />
                    </Route>
                    <Route path={ROUTES.MAP} exact>
                        <MapContainer t={t} />
                    </Route>
                    <Route path={ROUTES.LOGIN} exact>
                        <LoginContainer t={t} handleLogin={handleLogin} />
                    </Route>
                    <Redirect from={ROUTES.MAIN} to={ROUTES.GUIDES} />
                    <Route component={ErrorPage} />
                </Switch>
            </div>
        </Fragment>
    );
};

MainLayout.propTypes = {
    classes: PropTypes.shape({}).isRequired,
    t: PropTypes.func.isRequired,
    i18n: PropTypes.shape({}).isRequired
};

export default withRouter(withTranslation()(withStyles(styles)(MainLayout)));
