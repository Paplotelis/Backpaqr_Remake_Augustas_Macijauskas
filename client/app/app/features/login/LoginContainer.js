import React, { useContext, useEffect } from 'react';
import * as PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom'
import { withStyles, List, ListItem, Divider } from '@material-ui/core';

import GuidesContext from 'utils/contexts/guidesContext';

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
        margin: '5% auto'
    },
});

const LoginContainer = (props) => {
    const context = useContext(GuidesContext);

    const { classes } = props;

    useEffect(() => {
        context.getListOfGuidesAction();
    }, []);

    const { t, handleLogin } = props;

    if (context.guides === null) {
        return null;
    }

    return (
        <div className={classes.root}>
            <List component="nav" aria-label="User login">
                <Divider />
                {
                    context.guides.map(guide => {
                        return (
                            <React.Fragment key={guide.id}>
                                <ListItem
                                    onClick={() => handleLogin(guide)}
                                    button
                                    component='div'
                                >
                                    {`${guide.name}, ${t(`cities.${guide.city.replace(/\s/g,'')}`)}`}
                                </ListItem>
                                <Divider />
                            </React.Fragment>
                        );
                    })
                }
            </List>
        </div>
    );
};

LoginContainer.propTypes = {
    t: PropTypes.func.isRequired,
    handleLogin: PropTypes.func.isRequired,
};

export default withRouter(withStyles(styles)(LoginContainer));