import React from 'react';
import { Router } from 'react-router-dom';
import { I18nextProvider } from 'react-i18next';

import localization from 'core/localization';
import { history } from 'core';
import { MainLayout } from 'features/layout';
import { GuidesState } from 'features/guides';


const App = () => (
    <I18nextProvider i18n={localization}>
        <GuidesState>
            <Router history={history}>
                <MainLayout />
            </Router>
        </GuidesState>
    </I18nextProvider>
);

export default App;