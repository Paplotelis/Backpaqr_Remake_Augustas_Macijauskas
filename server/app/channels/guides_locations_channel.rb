class GuidesLocationsChannel < ApplicationCable::Channel
  def subscribed
    stream_for 'guides_locations_channel'
  end

  def received(data)
    GuidesLocationsChannel.broadcast_to("guides_locations_channel", data)
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
