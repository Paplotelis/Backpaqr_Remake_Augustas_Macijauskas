class LocationResource < ApplicationResource
  attribute :guide_id, :integer
  attribute :latitude, :big_decimal
  attribute :longitude, :big_decimal
  attribute :created_at, :datetime, writable: false
  attribute :updated_at, :datetime, writable: false
end
