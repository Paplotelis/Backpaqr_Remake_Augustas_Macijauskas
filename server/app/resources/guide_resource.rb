class GuideResource < ApplicationResource
  attribute :name, :string
  attribute :city, :string
  attribute :current_latitude, :big_decimal
  attribute :current_longitude, :big_decimal
  attribute :active, :boolean
  attribute :created_at, :datetime, writable: false
  attribute :updated_at, :datetime, writable: false
  has_many :locations
end
