class GuidesController < ApplicationController
  def index
    guides = GuideResource.all(params)
    respond_with(guides)
  end

  def show
    guide = GuideResource.find(params)
    respond_with(guide)
  end

  def create
    guide = GuideResource.build(params)

    if guide.save
      render jsonapi: guide, status: 201
    else
      render jsonapi_errors: guide
    end
  end

  def update
    guide = GuideResource.find(params)

    if guide.update_attributes
      render jsonapi: guide
      if params["data"]["attributes"]["current_latitude"] && params["data"]["attributes"]["current_longitude"]
        # Creating a new location for guide
        new_location = Location.create({
                                           guide_id: params["data"]["id"],
                                           latitude: params["data"]["attributes"]["current_latitude"],
                                           longitude: params["data"]["attributes"]["current_longitude"],
                                       })
        GuidesLocationsChannel.broadcast_to("guides_locations_channel", { guide: guide, location: new_location })
      else
        GuidesLocationsChannel.broadcast_to("guides_locations_channel", { guide: guide })
      end
    else
      render jsonapi_errors: guide
    end
  end

  def destroy
    guide = GuideResource.find(params)

    if guide.destroy
      render jsonapi: { meta: {} }, status: 200
    else
      render jsonapi_errors: guide
    end
  end
end
