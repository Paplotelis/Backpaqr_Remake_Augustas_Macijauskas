class CreateGuides < ActiveRecord::Migration[5.2]
  def change
    create_table :guides do |t|
      t.string :name
      t.string :city
      t.decimal :current_latitude
      t.decimal :current_longitude
      t.boolean :active
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end
  end
end
