Rails.application.routes.draw do
  scope path: ApplicationResource.endpoint_namespace, defaults: { format: :jsonapi } do
    resources :kebabs
    resources :guides
    resources :locations
    mount VandalUi::Engine, at: '/vandal'
    # your routes go here

    # Mounting action cable
    mount ActionCable.server, at: '/cable'

    # If there is a request to a route that does not exist, it is redirected to handled at application#routing_error
    match '*path', :to => 'application#routing_error', via: [:get, :post, :delete, :put, :patch]

  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
