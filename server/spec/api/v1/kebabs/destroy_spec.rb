require 'rails_helper'

RSpec.describe "kebabs#destroy", type: :request do
  subject(:make_request) do
    jsonapi_delete "/api/v1/kebabs/#{kebab.id}"
  end

  describe 'basic destroy' do
    let!(:kebab) { create(:kebab) }

    it 'updates the resource' do
      expect(KebabResource).to receive(:find).and_call_original
      expect {
        make_request
        expect(response.status).to eq(200), response.body
      }.to change { Kebab.count }.by(-1)
      expect { kebab.reload }
        .to raise_error(ActiveRecord::RecordNotFound)
      expect(json).to eq('meta' => {})
    end
  end
end
