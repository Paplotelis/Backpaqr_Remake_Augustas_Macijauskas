require 'rails_helper'

RSpec.describe "kebabs#show", type: :request do
  let(:params) { {} }

  subject(:make_request) do
    jsonapi_get "/api/v1/kebabs/#{kebab.id}", params: params
  end

  describe 'basic fetch' do
    let!(:kebab) { create(:kebab) }

    it 'works' do
      expect(KebabResource).to receive(:find).and_call_original
      make_request
      expect(response.status).to eq(200)
      expect(d.jsonapi_type).to eq('kebabs')
      expect(d.id).to eq(kebab.id)
    end
  end
end
