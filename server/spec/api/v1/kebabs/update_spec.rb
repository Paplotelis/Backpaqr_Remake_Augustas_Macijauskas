require 'rails_helper'

RSpec.describe "kebabs#update", type: :request do
  subject(:make_request) do
    jsonapi_put "/api/v1/kebabs/#{kebab.id}", payload
  end

  describe 'basic update' do
    let!(:kebab) { create(:kebab) }

    let(:payload) do
      {
        data: {
          id: kebab.id.to_s,
          type: 'kebabs',
          attributes: {
            # ... your attrs here
          }
        }
      }
    end

    # Replace 'xit' with 'it' after adding attributes
    xit 'updates the resource' do
      expect(KebabResource).to receive(:find).and_call_original
      expect {
        make_request
        expect(response.status).to eq(200), response.body
      }.to change { kebab.reload.attributes }
    end
  end
end
