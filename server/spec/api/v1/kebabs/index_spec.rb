require 'rails_helper'

RSpec.describe "kebabs#index", type: :request do
  let(:params) { {} }

  subject(:make_request) do
    jsonapi_get "/api/v1/kebabs", params: params
  end

  describe 'basic fetch' do
    let!(:kebab1) { create(:kebab) }
    let!(:kebab2) { create(:kebab) }

    it 'works' do
      expect(KebabResource).to receive(:all).and_call_original
      make_request
      expect(response.status).to eq(200), response.body
      expect(d.map(&:jsonapi_type).uniq).to match_array(['kebabs'])
      expect(d.map(&:id)).to match_array([kebab1.id, kebab2.id])
    end
  end
end
