FactoryBot.define do
  factory :location do
    guide_id { 1 }
    latitude { "9.99" }
    longitude { "9.99" }
  end
end
