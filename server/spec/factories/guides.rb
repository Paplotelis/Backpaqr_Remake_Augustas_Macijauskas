FactoryBot.define do
  factory :guide do
    name { "MyString" }
    city { "MyString" }
    current_latitude { "9.99" }
    current_longitude { "9.99" }
    active { false }
    created_at { "2019-06-16 16:15:39" }
    updated_at { "2019-06-16 16:15:39" }
  end
end
