FactoryBot.define do
  factory :kebab do
    name { "MyString" }
    city { "MyString" }
    current_latitude { "9.99" }
    current_longitude { "9.99" }
    active { false }
    created_at { "2019-06-16 16:28:28" }
    updated_at { "2019-06-16 16:28:28" }
  end
end
