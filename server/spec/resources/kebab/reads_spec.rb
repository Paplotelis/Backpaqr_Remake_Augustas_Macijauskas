require 'rails_helper'

RSpec.describe KebabResource, type: :resource do
  describe 'serialization' do
    let!(:kebab) { create(:kebab) }

    it 'works' do
      render
      data = jsonapi_data[0]
      expect(data.id).to eq(kebab.id)
      expect(data.jsonapi_type).to eq('kebabs')
    end
  end

  describe 'filtering' do
    let!(:kebab1) { create(:kebab) }
    let!(:kebab2) { create(:kebab) }

    context 'by id' do
      before do
        params[:filter] = { id: { eq: kebab2.id } }
      end

      it 'works' do
        render
        expect(d.map(&:id)).to eq([kebab2.id])
      end
    end
  end

  describe 'sorting' do
    describe 'by id' do
      let!(:kebab1) { create(:kebab) }
      let!(:kebab2) { create(:kebab) }

      context 'when ascending' do
        before do
          params[:sort] = 'id'
        end

        it 'works' do
          render
          expect(d.map(&:id)).to eq([
            kebab1.id,
            kebab2.id
          ])
        end
      end

      context 'when descending' do
        before do
          params[:sort] = '-id'
        end

        it 'works' do
          render
          expect(d.map(&:id)).to eq([
            kebab2.id,
            kebab1.id
          ])
        end
      end
    end
  end

  describe 'sideloading' do
    # ... your tests ...
  end
end
