require 'rails_helper'

RSpec.describe KebabResource, type: :resource do
  describe 'creating' do
    let(:payload) do
      {
        data: {
          type: 'kebabs',
          attributes: attributes_for(:kebab)
        }
      }
    end

    let(:instance) do
      KebabResource.build(payload)
    end

    it 'works' do
      expect {
        expect(instance.save).to eq(true), instance.errors.full_messages.to_sentence
      }.to change { Kebab.count }.by(1)
    end
  end

  describe 'updating' do
    let!(:kebab) { create(:kebab) }

    let(:payload) do
      {
        data: {
          id: kebab.id.to_s,
          type: 'kebabs',
          attributes: { } # Todo!
        }
      }
    end

    let(:instance) do
      KebabResource.find(payload)
    end

    xit 'works (add some attributes and enable this spec)' do
      expect {
        expect(instance.update_attributes).to eq(true)
      }.to change { kebab.reload.updated_at }
      # .and change { kebab.foo }.to('bar') <- example
    end
  end

  describe 'destroying' do
    let!(:kebab) { create(:kebab) }

    let(:instance) do
      KebabResource.find(id: kebab.id)
    end

    it 'works' do
      expect {
        expect(instance.destroy).to eq(true)
      }.to change { Kebab.count }.by(-1)
    end
  end
end
